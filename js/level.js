Level = function(game, player) {

  // Game
  this.game = game;

  this.player = player;

  this.items = null;

  this.platforms = null;

  // Assets
  this.assets = {
    background: 'background.png',
    ground: 'ground.png'
  };

  this.size = {x: 600, y: 20000};

  this.tileSize = 32;

  this.y = 0;
  this.x = 0;

  this.moon = null;
  this.earth = null;
};

Level.prototype = {

    preload: function() {
      for(key in this.assets){
        this.game.load.image(key, 'assets/' + this.assets[key]);
      }
    },

    create: function() {

      // Set the world bounds to 600 wide, 10000 high
      game.world.setBounds(0, 0, this.size.x, this.size.y);

      this.y = game.world.bounds.height - 100;
      this.x = (Math.random() * game.world.bounds.width-32)+this.tileSize;

      // Set the background image to tile
      game.add.tileSprite(0, 0, game.world.bounds.width, game.world.bounds.height, 'background');

      this.earth = game.add.sprite(0, game.world.bounds.height-85, 'earth');

      this.moon = game.add.sprite(0, game.world.bounds.height-600, 'moon');

      // Fixed Platforms
      platforms = game.add.group();
      platforms.enableBody = true;

      // Items
      items = game.add.group();
      items.enableBody = true;

      // Enemies
      enemies = game.add.group();
      enemies.enableBody = true;


      this.createSection();



      ground = game.add.group();
      ground.enableBody = true;
      // Add full width ground at bottom
      var bottom = game.add.tileSprite(0, game.world.bounds.height - this.tileSize, game.world.bounds.width, this.tileSize, 'metalTile');
      ground.add(bottom);

      bottom.body.allowGravity = false;
      bottom.body.immovable = true;
      bottom.body.moves = false;

      //enemies.setAll('body.collideWorldBounds', true);


      //game.physics.arcade.collide(player, enemies);
      //game.physics.arcade.collide(enemies, platforms);

    },

    update: function() {

      this.moon.y = this.game.camera.y;
      this.moon.x = this.game.camera.x;
      this.earth.y = this.game.camera.y+550;
      this.earth.x = this.game.camera.x;
      if(player.body.y - this.y < 200)
        this.createSection();

    }

};

Level.prototype.createSection = function(){
  // Generate platform
  for(var h = 0; h < 10; ++h){

    for(var i = 0; i < 3; ++i){
      var chance = Math.random();
      if(chance  < 0.6){

        var p = new Platform(game, this.x, this.y, Math.floor((Math.random()*4)+1)*this.tileSize, this.tileSize, 'metalTile', platforms, 0, "x");
        p.create();

        platforms.add(p);

      }else if(chance < 0.7){

        var p = new Platform(game, this.x, this.y, Math.floor((Math.random()*4)+1)*this.tileSize, this.tileSize, 'moveTile', platforms, 100, "x");
        p.create();

        platforms.add(p);
        var spaceTaken = false;
      }else if(chance < 0.9){

        var p = new Platform(game, this.x, this.y, Math.floor((Math.random()*4)+1)*this.tileSize, this.tileSize, 'moveTile', platforms, 50, "y");
        p.create();

        platforms.add(p);
      }

      // add item randomly
      if(Math.random()*100 < 60){
        var type =  Math.floor(Math.random()*3);
        var it = items.create(this.x, this.y-24, 'crystals', type);
        it.body.gravity.y = 6;
        it.body.bounce.y = 0.7 + Math.random()*0.2;
        it.pickup = type;
        items.add(it);
        spaceTaken = true;
      }

      // add enemy randomly
      if(Math.random()*100 < 60){
        var enemy = new Enemy(game, this.x, this.y-24, 'enemy');
        enemies.add(enemy);
        enemies.create();

      }

      this.x += ((Math.random()*10)-5)*32;
      if(this.x < 0)
        this.x += (Math.random()*50)*32;
      else if(this.x > 800)
        this.x -= (Math.random()*50)*32;

    }

    this.y -= 90;
  }
}
