// Player class
// inherits from Sprite class

Player = function (game, x, y, image) {

  this.timers = { slide: 0,
                  jump: 0,
                  walk: 0,
                  dust: 0,
                  pickup: 0,
                  walltouch: 0,
                  walljump: 0,
                  attack: 0,
                  kill: 0,
                  bullet: 0,
                  jet: 0
                };

  this.score = 0;

  this.scoreText = null;

  this.life = 200;

  this.lifeText = null;

  this.numBullets = 10;

  this.jetpackFuel = 100;

  this.fuelText = null;

  this.facing = 1;
  this.walkSpeed = 150;
  this.walkAccel = 3;
  this.glideAccel = 2;
  this.jumpSpeed = -150;
  this.doubleJumpSpeed = -100;
  this.walljumpSpeed = 30;
  this.stompSpeed = 300;
  this.wallslideSpeed = 4;
  this.slideFriction = 4;
  this.turnFriction = 10;
  this.floorDrag = 3;
  this.airDrag = 2;
  this.wallDrag = 8;
  this.bulletSpeed = 250;

  // flags
  this.canDoublejump = false;
  this.canWalljump = true;
  this.canSlash = true;

  // sub sprites
  this.attack = null;

  this.bulletPool = null;

  this.gun = {x: 8, y: 17};

  // call super
  Phaser.Sprite.call(this, game, x, y, 'player', 0);

  // set anchor to middle top
  this.anchor.setTo(0.5, 0);



  // Animations
  /*
  this.animations.add('walk', Phaser.Animation.generateFrameNames(image, 0, 3, '', 4), 10, true);
  this.animations.add('run', Phaser.Animation.generateFrameNames(image, 14, 17, '', 4), 10, true);
  this.animations.add('jump', Phaser.Animation.generateFrameNames(image, 4, 7, '', 4), 4, true);
  this.animations.add('climb', Phaser.Animation.generateFrameNames(image, 18, 21, '', 4), 10, true);
  this.animations.add('hit', [image+'0008', image+'0009', image+'0008'], 10, true, false);
  this.animations.add('slash', [image+"0011", image+"0010", image+"0011", image+"0012"], 10, true);
  this.animations.add('hit', [image+'0011', image+'0013', image+'0011'], 10, true);
  this.animations.add('idle', [image+'0006', image+'0007'], 2, true);
  this.animations.add('slide', [image+'0007'], 1, true);
  this.animations.add('wallslide', [image+'0006'], 1, true);
  */
  this.animations.add('walk', Phaser.Animation.generateFrameNames(image, 1, 5, '', 4), 10, true);
  this.animations.add('jump', Phaser.Animation.generateFrameNames(image, 6, 8, '', 4), 4, true);
  this.animations.add('shoot', Phaser.Animation.generateFrameNames(image, 9, 11, '', 4), 10, true);
  this.animations.add('idle', [image+'0000', image+'0006'], 2, true);
  this.animations.add('slide', [image+'0006'], 1, true);
  this.animations.add('wallslide', [image+'0008'], 1, true);

  // Animation and other durations
  this.durations = {};
  this.durations.jump = 1000;
  this.durations.walk = 100;
  this.durations.dust = 100;
  this.durations.slide = 200;
  this.durations.pickup = 500;
  this.durations.walltouch = 20;
  this.durations.walljump = 100;
  this.durations.attack = 500;
  this.durations.kill = 1000;
  this.durations.shot = 100;
  this.durations.jet = 100;

  // Particles
  this.dustEmitter = game.add.emitter(0, 0, 100);
  this.dustEmitter.makeParticles(['dust']);
  this.dustEmitter.gravity = -400;
  this.dustEmitter.setAlpha(1, 0, 1000);
  this.dustEmitter.setScale(0.5, 1, 0.5, 1, 1000); // minx, maxx, miny, maxy, rate, ease, [yoyo]
  this.dustEmitter.start(false, 1000, 50);
  this.dustEmitter.on = false;

  // Item pickup
  this.pickupEmitter = game.add.emitter(0, 0, 500);
  this.pickupEmitter.makeParticles(['sparks']);
  this.pickupEmitter.gravity = 5;
  this.pickupEmitter.setAlpha(1, 0, 1000);
  this.pickupEmitter.setScale(1, 3, 1, 3, 1000); // minx, maxx, miny, maxy, rate, ease, [yoyo]
  this.pickupEmitter.start(true, 1000, 50);
  this.pickupEmitter.on = false;

  // Kill
  this.killEmitter = game.add.emitter(0, 0, 500);
  this.killEmitter.makeParticles(['fireball']);
  this.killEmitter.gravity = 0;
  this.killEmitter.setAlpha(1, 0, 1000);
  this.killEmitter.setScale(1, 3, 1, 3, 1000); // minx, maxx, miny, maxy, rate, ease, [yoyo]
  this.killEmitter.start(true, 1000, 50);
  this.killEmitter.on = false;

  this.jetEmitter = game.add.emitter(0, 0, 500);
  this.jetEmitter.makeParticles(['fireball']);
  this.jetEmitter.gravity = 0;
  this.jetEmitter.setAlpha(1, 0, 1000);
  this.jetEmitter.setScale(1, 3, 1, 3, 1000); // minx, maxx, miny, maxy, rate, ease, [yoyo]
  this.jetEmitter.start(false, 1000, 50);
  this.jetEmitter.on = false;

  // Sounds
  this.sounds = game.add.audio('sfx');
  this.sounds.addMarker('pickup', 0, 0.37);
  this.sounds.addMarker('jump', 2, 0.37);
  this.sounds.addMarker('slide', 4, 0.3);
  this.sounds.addMarker('thud', 6, 0.6);
  this.sounds.addMarker('squash', 8, 0.8);
  this.sounds.addMarker('attack', 10, 0.8);
  this.sounds.addMarker('attack2', 12, 0.8);

  this.jetSound = game.add.audio('jet');
  this.deathSound = game.add.audio('death');


  // add sprite to game
  game.add.existing(this);


};

// Inherit
Player.prototype = Object.create(Phaser.Sprite.prototype);
Player.prototype.constructor = Player;

// Preload
Player.prototype.preload = function(){
  // collisions

}

Player.prototype.create = function(){



  this.bulletPool = this.game.add.group();
  for(var i = 0; i < this.numBullets; ++i){
    var bullet =  this.game.add.sprite(0, 0, 'bullet');
    this.bulletPool.add(bullet);
    bullet.anchor.setTo(0.5, 0.5);
    this.game.physics.enable(bullet, Phaser.Physics.ARCADE);
    bullet.body.allowGravity = false;
    bullet.kill();
  }

  this.scoreText = game.add.text(50, game.world.centerY, "Score: " + this.score, {
      font: "22px Arial",
      fill: "#ffffff",
      align: "center"
  });

  this.scoreText.anchor.setTo(0.5, 0.5);

  this.fuelText = game.add.text(250, game.world.centerY, "Fuel: " + this.jetpackFuel, {
      font: "22px Arial",
      fill: "#ffffff",
      align: "center"
  });

  this.fuelText.anchor.setTo(0.5, 0.5);

  this.lifeText = game.add.text(450, game.world.centerY, "Health: " + this.life, {
      font: "22px Arial",
      fill: "#ffffff",
      align: "center"
  });

  this.lifeText.anchor.setTo(0.5, 0.5);

  //this.gun = this.game.add.sprite(32, -8);
  //this.gun.anchor.setTo(0.5, 0.5);
  //this.addChild(this.gun);
}

Player.prototype.beforeCollide = function(player, other){
    if(player.body.velocity.y >= player.stompSpeed){
      player.sounds.play('thud');
    }
}

Player.prototype.collideEnemy = function(player, enemy){

  if(player.body.bottom < enemy.body.y){
    player.stompEnemy();
  }else{
    player.life -= 10;
    player.lifeText.setText('Health: ' + player.life);
    player.body.velocity.x = (player.body.x - enemy.body.x)/Math.abs(player.body.x - enemy.body.x)*100;
  }
}

Player.prototype.stompEnemy = function(player, enemy){
  if(player.body.velocity.y >= 100){
    player.sounds.play('squash');
    enemy.kill();
    player.killEnemy(enemy);
  }
}

Player.prototype.shootEnemy = function(bullet, enemy){
  enemy.damage(10);
  player.killEnemy(enemy);
  bullet.kill();
}

Player.prototype.killBullet = function(bullet, object){
  bullet.kill();
}

Player.prototype.killEnemy = function(enemy){
  if(!enemy.alive){
    this.killEmitter.setXSpeed(-10, 10);
    this.killEmitter.setYSpeed(-10, 10);
    this.killEmitter.at(enemy);
    //this.dieEmitter.emitY += 16;
    //this.dustEmitter.emitX += 8;
    this.killEmitter.on = true;

    this.timers.kill = game.time.now + this.durations.kill;

    player.score += 50;
  }
}

Player.prototype.die = function(){

  console.log('died');
  theme.stop();
  this.deathSound.play();
  this.kill();
}

// Update
Player.prototype.update = function() {

  if(this.alive){
    if(this.life <= 0){
      this.die();
    }
  }else{
    return;
  }

  // Collisons
  game.physics.arcade.collide(player, platforms, null, this.beforeCollide, this);
  game.physics.arcade.collide(items, player, player.pickupItem);
  game.physics.arcade.collide(this, enemies, this.collideEnemy);
  game.physics.arcade.collide(this.bulletPool, platforms, this.killBullet);
  game.physics.arcade.collide(this.bulletPool, enemies, this.shootEnemy);
  game.physics.arcade.collide(this, ground);

  // if we touched a wall, set the walltouch timer
  // used in walljumping
  if(this.body.touching.left || this.body.touching.right)
    this.timers.walltouch = game.time.now + this.durations.walltouch;

/*
  if(!this.doubleJump && this.onFloor()){
    this.doubleJump = true;
  }
*/

  this.scoreText.x = game.camera.x+50;
  this.scoreText.y = game.camera.y+25;
  this.fuelText.x = game.camera.x+250;
  this.fuelText.y = game.camera.y+25;
  this.lifeText.x = game.camera.x+450;
  this.lifeText.y = game.camera.y+25;



    // Left and Right keys
//==================
// <==== Left
//==================
    if(this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT) || this.game.input.keyboard.isDown(Phaser.Keyboard.A)){

      // we were facing the other way
      if(this.facing == 1){
        // turn the sprite
        this.facing = -1;

        // on floor
        if(this.onFloor()){
// Slide while turning
          if(this.body.velocity.x > this.walkSpeed*0.75){

            this.slide();
          }
// Turn
          else if(this.body.velocity.x > 0){

            this.turn();
          }
        }
        // not on floor
        else{
          // jumping away from wall
// Walljump
          if(this.timers.walltouch > game.time.now){
            this.walljump();
          }
// Falling
          else
            this.glide();
        }

      }
      // heading the same direction
      else{
        // not on floor
        if(!this.onFloor()){
          // on wall
// Wallslide
          if(this.body.touching.left)
            this.wallslide();
// Falling
          else
            this.glide();
        }
// Walking
        else{
          this.walk();
        }
      }

    }
//==================
// Right ===>
//==================
    else if (this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT) || this.game.input.keyboard.isDown(Phaser.Keyboard.D)){

      // we were facing the other way
      if(this.facing == -1){
        // turn the sprite
        this.facing = 1;

        // on floor
        if(this.onFloor()){
// Slide
          if(this.body.velocity.x < -(this.walkSpeed*0.75)){
            this.slide();
          }
// Turn
          else if(this.body.velocity.x < 0){
            this.turn();
          }
        }
        // not on floor
        else{
          // jumping away from wall
// Walljump
          if(this.timers.walltouch > game.time.now){
            this.walljump();
          }
          // falling
          else{
            this.glide();
          }
        }

      }
      // heading the same direction
      else{
        // not on floor
        if(!this.onFloor()){
          // on wall
// Wallslide
          if(this.body.touching.right)
            this.wallslide();
// Falling
          else
            this.glide();
        }
        // set state to walking
// Walk
        else
          this.walk();
      }
    }
// Idle
    else{
      this.idle();
    }

    // Up and Down keys
//================
// Up ^
//================
    if(this.game.input.keyboard.isDown(Phaser.Keyboard.UP) || this.game.input.keyboard.isDown(Phaser.Keyboard.W)){

      if(this.onFloor()){
        this.jump();
      }
      else if( this.doubleJump && this.timers.jump - game.time.now < 800){
        this.doublejump();
      }

      else if(this.onWall()){
        this.walljump();
      }
    }
    else if(this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN) || this.game.input.keyboard.isDown(Phaser.Keyboard.S)){
      if(this.onFloor()){
        if(this.body.velocity.x > this.walkSpeed*0.75 || this.body.velocity.x < -(this.walkSpeed*0.75)){
          this.slide();
        }
        else{
          this.crouch();
        }
      }
      else{
        this.stomp();
      }
    }

    // Attack
    if(this.game.input.keyboard.isDown(Phaser.Keyboard.F)){
      if(this.attack === null && !this.game.input.keyboard.justPressed(Phaser.Keyboard.F, 50)){
        this.shoot();
      }
    }

    // Jetpack
    if(this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)){
      if( this.jetpackFuel > 0){
        this.jet();
      }
    }

    // Timed events
    if(this.timers.dust < game.time.now){
      this.dustEmitter.on = false;
    }

    if(this.timers.pickup < game.time.now){
      this.pickupEmitter.on = false;
    }

    if(this.timers.kill < game.time.now){
      this.killEmitter.on = false;
    }

    if(this.timers.jet < game.time.now){
      this.jetEmitter.on = false;
    }

    if(this.attack && this.timers.attack < game.time.now){
      this.attack.destroy();
      this.attack = null;
    }

}; // Update Ends

//============================================================================
// Utility Functions
//============================================================================
Player.prototype.onFloor = function(){
    if(this.body.touching.down)
      return true;

    return false;
};

Player.prototype.onWall = function(){
    if(this.body.touching.left || this.body.touching.right)
      return true;

    return false;
};

Player.prototype.pickupItem = function(player, item){

  switch(item.pickup){
    case 0:
      player.score += 10;
      player.scoreText.setText('Score: ' + player.score);
      break;

    case 1:
      player.jetpackFuel += 50;
      player.fuelText.setText('Fuel: ' + player.jetpackFuel);
      break;

    case 2:
      player.life += 10;
      player.lifeText.setText('Health: ' + player.life);
      break;
  }


  // emitter
  player.pickupEmitter.setXSpeed(0, -player.body.velocity.x);
  player.pickupEmitter.setYSpeed(0, 10);
  player.pickupEmitter.at(item);
  //player.pickupEmitter.emitY += 16;
  //player.dustEmitter.emitX += 8;
  player.pickupEmitter.on = true;

  player.sounds.play('pickup');


  player.timers.pickup = game.time.now + player.durations.pickup;

  item.destroy();
}

//============================================================================
// Movement Functions
//============================================================================
Player.prototype.slide = function(){

  this.animations.play('slide');

  var velx = Math.abs(this.body.velocity.x);

  if(velx > 0)
    this.body.velocity.x += this.slideFriction*-(velx/this.body.velocity.x);

  this.dustEmitter.setXSpeed(0, -this.body.velocity.x);
  this.dustEmitter.setYSpeed(0, 10);
  this.dustEmitter.at(this);
  this.dustEmitter.emitY += 16;
  //this.dustEmitter.emitX += 8;
  this.dustEmitter.on = true;

  if(this.timers.slide < game.time.now)
    this.sounds.play('slide');

  this.timers.dust = game.time.now + this.durations.dust;

  this.timers.slide = game.time.now + this.durations.slide;

};

Player.prototype.turn = function(){
  var velx = Math.abs(this.body.velocity.x);
  if(velx > 5){
    this.body.velocity.x += this.turnFriction*-(velx/this.body.velocity.x);
  }
};

Player.prototype.jump = function(){

  var anim = this.animations.play('jump');
  this.body.velocity.y = this.jumpSpeed;

  this.sounds.play('jump');

  this.timers.jump = this.game.time.now + this.durations.jump;

};

Player.prototype.jet = function(){
  console.log('jet');

  if(this.timers.jet < game.time.now){
    this.timers.jet = game.time.now + this.durations.jet;
    this.jetSound.play();
  }

  this.jetEmitter.setXSpeed(-10,10);
  this.jetEmitter.setYSpeed(this.body.velocity.y, 100);
  this.jetEmitter.at(this);
  this.jetEmitter.emitY += 20;
  this.jetEmitter.emitX += 16*-this.facing;
  this.jetEmitter.on = true;

  this.body.velocity.y -= 10;
  this.jetpackFuel -= 1;
  this.fuelText.setText('Fuel: ' + player.jetpackFuel);
}

Player.prototype.walljump = function(){

  if(this.timers.walljump < game.time.now){
    this.timers.walljump = game.time.now + this.durations.walljump;
    return;
  }

  if(this.animations.currentAnim.name !== 'jump')
    this.animations.play('jump');


  var sign = this.body.touching.right ? -1 : 1;

  this.body.velocity.x = this.walljumpSpeed*sign;

  this.body.velocity.y = this.jumpSpeed;

  this.sounds.play('jump');



  this.dustEmitter.setXSpeed(-10,10);
  this.dustEmitter.setYSpeed(-this.body.velocity.y, 0);
  this.dustEmitter.at(this);
  this.dustEmitter.emitY += 16;
  //this.dustEmitter.emitX += 8*this.facing;
  this.dustEmitter.on = true;
  this.timers.dust = game.time.now + this.durations.dust;
};

Player.prototype.doublejump = function(){

  this.doubleJump = false;

  console.log(this.doubleJump);

  this.animations.play('jump');

  this.body.velocity.y += this.doubleJumpSpeed;

  this.sounds.play('jump');

  this.timers.jump = this.game.time.now + this.durations.jump;
}

Player.prototype.wallslide = function(){

  if(this.animations.currentAnim.name !== 'wallslide')
    this.animations.play('wallslide');

  if(this.body.velocity.y > this.wallslideSpeed)
  this.body.velocity.y -= this.wallDrag;

  this.dustEmitter.setXSpeed(-10,10);
  this.dustEmitter.setYSpeed(-this.body.velocity.y, 0);
  this.dustEmitter.at(this);
  this.dustEmitter.emitY += 16;
  //this.dustEmitter.emitX += 8*this.facing;
  this.dustEmitter.on = true;
  this.timers.dust = game.time.now + this.durations.dust;
};

Player.prototype.glide = function(){

  if(this.scale.x != this.facing)
    this.scale.x = this.facing;

  var velx = Math.abs(this.body.velocity.x);
  if(velx < this.walkSpeed){
    this.body.velocity.x += this.glideAccel * this.facing;
  }
};

Player.prototype.stomp = function(){

  this.body.velocity.y = this.stompSpeed;
  this.animations.play('slide');

};

Player.prototype.crouch = function(){

  var velx = Math.abs(this.body.velocity.x);

  if(velx > 0)
    this.body.velocity.x += this.slideFriction*-(velx/this.body.velocity.x);

  this.animations.play('slide');

};

Player.prototype.walk = function(){

  // play the walk animation
  if(this.animations.currentAnim.name !== 'walk'){
    if(this.timers.slide < game.time.now)
      this.animations.play('walk');
  }

  this.timers.walk = game.time.now + this.durations.walk;

  // set the sprite to face the direction we're heading
  if(this.scale.x != this.facing)
    this.scale.x = this.facing;

  var velx = Math.abs(this.body.velocity.x);

  // increase the x velocity if not at max
  if(velx < this.walkSpeed)
    this.body.velocity.x += this.walkAccel * this.facing;

};

Player.prototype.idle = function(){
  this.animations.stop();

  var velx = Math.abs(this.body.velocity.x);

  // only start idling when standing on the floor
  if(this.onFloor()){
    this.animations.play('idle');
    this.body.velocity.x += this.floorDrag*-(velx/this.body.velocity.x);
  }
  else
    this.body.velocity.x += this.airDrag*-(velx/this.body.velocity.x);

  if(velx < 5)
    this.body.velocity.x = 0;
};

Player.prototype.slash = function(){
  this.animations.play('slash');

  this.sounds.play('attack');

  this.attack = this.game.add.sprite(0, -8, 'attack');
  this.attack.anchor.setTo(0.5, 0);
  this.attack.animations.add('go');
  this.attack.animations.play('go', 8, false);
  this.addChild(this.attack);

  this.timers.attack = game.time.now + this.durations.attack;
}

Player.prototype.shoot = function(){
  if(this.timers.bullet > game.time.now){
    return;
  }


  this.sounds.play('attack');

  console.log('shoot')
  this.timers.bullet = game.time.now + this.durations.bullet;

  var bullet = this.bulletPool.getFirstDead();

  console.log(bullet)
  if(bullet === null || bullet === undefined){
    return;
  }

  this.animations.play('shoot');

  bullet.revive();

  bullet.checkWorldBounds = true;
  bullet.outOfBoundsKill = true;

  bullet.reset(this.body.x + this.gun.x, this.body.y + this.gun.y);

  bullet.scale.x = this.facing;
  bullet.body.velocity.x = this.bulletSpeed*this.facing;
  bullet.body.velocity.y = (Math.random()*6)-3;
}
