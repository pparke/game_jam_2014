// Platform Class
// Inherits from Sprite class
Platform = function(game, x, y, width, height, image, group, speed, axis) {

  this.name = "platform";
  this.speed = speed;
  this.axis = axis;
  this.group = group;

  Phaser.TileSprite.call(this, game, x, y, width, height, image);


}

// Inherit
Platform.prototype = Object.create(Phaser.TileSprite.prototype);
Platform.prototype.constructor = Platform;

Platform.prototype.create = function(){

  //game.physics.arcade.enable(this);

  this.group.add(this);
  this.anchor.set(0.5);
  this.body.immovable = true
  this.body.checkCollision.down = false;
  this.body.allowGravity = false;
  this.body.bounce.set(1);
  this.body.velocity[this.axis] = this.speed;
  this.body.collideWorldBounds = true;
}

// Update
Platform.prototype.update = function(){

  // If this platform is supposed to
  // move back and forth on the x-axis
  /*
  if(this.axis == "x")
  {

    // If the tiles x position is less than
    // the provided minimum, change the direction
    if(this.position.x < this.min) this.direction *= -1;

    // If the tiles right side is less than the
    // the provided maximum, change the direction
    if(this.body.right > this.max) this.direction *= -1;

    // Move the platform in its current
    // direction at the privided velocity
    this.position.x =  this.position.x + (this.speed * this.direction);

  }

  else if(this.axis == "y")
  {
    // If the tiles y position is less than
    // the provided minimum, change the direction
    if(this.body.bottom < this.min) this.direction *= -1;

    // If the tiles y position is less than the
    // the provided maximum, change the direction
    if(this.position.y > this.max) this.direction *= -1;

    // Move the platform in its current
    // direction at the privided velocity
    this.position.y =  this.position.y + (this.speed * this.direction);
  }
  */

}
