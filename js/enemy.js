// Enemy Class
// Inherits from Sprite class
Enemy = function(game, x, y, image) {

  this.name = "enemy";

  // Timers for ongoing animations
  this.timers = {
    walk: 0,
    jump: 0,
    attack: 0,
    checkPlatform: 0
  };

  // Durations of animations
  this.durations = {
    walk: 100,
    jump: 100,
    attack: 100
  };

  // The speed at which
  // the enemy will walk
  this.walkSpeed = 75;

  // The acceleration at which
  // the enemy will undergo while walking
  this.walkAccell = 5;

  this.jumpSpeed = -300;

  // The distance the enemy must be within
  // of the player before started an attack
  this.attackDistance = 40;

  // Call the parent sprite constructor
  Phaser.Sprite.call(this, game, x, y, 'enemy', 0);

  // Set enemy rotation anchor
  this.anchor.setTo(0.5, 0);

  // Define animations for all enemy states
  /*
  this.animations.add('walk', Phaser.Animation.generateFrameNames(image, 0, 3, '', 4), 10, true);
  this.animations.add('run', Phaser.Animation.generateFrameNames(image, 14, 17, '', 4), 10, true);
  this.animations.add('jump', Phaser.Animation.generateFrameNames(image, 4, 7, '', 4), 4, true);
  this.animations.add('climb', Phaser.Animation.generateFrameNames(image, 18, 21, '', 4), 10, true);
  this.animations.add('hit', [image+'0008', image+'0009', image+'0008'], 10, true, false);
  this.animations.add('slash', [image+"0011", image+"0010", image+"0011", image+"0012"], 10, true);
  this.animations.add('hit', [image+'0011', image+'0013', image+'0011'], 10, true);
  this.animations.add('idle', [image+'0006', image+'0007'], 2, true);
  this.animations.add('slide', [image+'0007'], 1, true);
  this.animations.add('wallslide', [image+'0006'], 1, true);
*/

  this.animations.add('walk', Phaser.Animation.generateFrameNames(image, 1, 4, '', 4), 10, true);
  //this.animations.add('jump', Phaser.Animation.generateFrameNames(image, 6, 8, '', 4), 4, true);
  this.animations.add('shoot', Phaser.Animation.generateFrameNames(image, 5, 8, '', 4), 10, true);
  this.animations.add('idle', [image+'0000', image+'0001'], 2, true);

  //game.physics.arcade.enable(this);

  this.onPlatform = false;
  this.platform = {body: {x:0, right:0}};


}

// Inherit
Enemy.prototype = Object.create(Phaser.Sprite.prototype);
Enemy.prototype.constructor = Enemy;

// Preload
Enemy.prototype.preload = function(){

}

Enemy.prototype.setPlatform = function(self, platform){
  self.platform = platform;
  self.onPlatform = true;
}

Enemy.prototype.create = function(){

  this.health = 100;

  this.body.collideWorldBounds = true;

  this.body.setSize(14, 30, 8, 10);
}


// Update
Enemy.prototype.update = function(){

  if(Math.abs(this.body.y - player.body.y) > 2000){
    this.destroy();
    return;
  }

  if(this.body.velocity.y > 5){
    //this.timers.checkPlatform = game.time.now + 1000;
      if(!this.onFloor()){
        this.onPlatform = false;
      }
  }

  // Collisions
  if(!this.onPlatform)
    game.physics.arcade.collide(this, platforms, this.setPlatform);
  else
    game.physics.arcade.collide(this, platforms);
  game.physics.arcade.collide(this, ground);
  game.physics.arcade.collide(this, enemies);



  // Start checking if interaction from an enemy
  // is required if the player if within 90px of the enemy
  if(game.physics.arcade.distanceBetween(this, player) < 90){

    // If the player is within 5px of the enemy on the x-axis,
    // but is on a platform above the enemy, jump at the player.
    if(
      this.isWithinXOf(15, player) &&
      this.isAtleastYAbove(32, player) &&
      this.onFloor()
    ){
      this.jump();
    }

    // If the player is on the same level as the enemy and is
    // within attack distance on the x-axis, attack the player
    else if(
      this.isAtmostYAbove(10, player) &&
      this.isAtmostYBelow(10, player) &&
      this.isWithinXOf(this.attackDistance, player)
    ){
      this.attack(player);
    }

    // If no other interaction conditions are
    // met, walk in the direction of the player
    else {
      if(this.position.x > player.position.x){
        if(this.canWalkLeftWithoutFalling()){
          console.log('walking left');
          this.walk(-1);
        }else{
          this.idle();
        }
      }
      else if(this.position.x < player.position.x){
        if(this.canWalkRightWithoutFalling()){
          this.walk(1);
        }else{
          this.idle();
        }
      }
    }

  // Otherwise, if the player is not
  // within 90px of the enemy, idle.
  }else{
    this.idle();
  }

}

// Returns true if the enemy is atleast a
// certain number of pixels above the provided sprite
Enemy.prototype.isAtleastYAbove = function(px, sprite){
  return (this.position.y - sprite.position.y > px);
}

// Returns true if the enemy is at most a
// certain number of pixels above the provided sprite
Enemy.prototype.isAtmostYAbove = function(px, sprite){
  return (this.position.y - sprite.position.y < px)
}

// Returns true if the enemy is atleast a
// certain number of pixels below the provided sprite
Enemy.prototype.isAtleastYBelow = function(px, sprite){
  return (sprite.position.y - this.position.y > px);
}

// Returns true if the enemy is at most a
// certain number of pixels below the provided sprite
Enemy.prototype.isAtmostYBelow = function(px, sprite){
  return (sprite.position.y - this.position.y < px);
}

// Returns true if the enemy is within a certain
// number of pixels of the provided sprite on the x-axis
Enemy.prototype.isWithinXOf = function(px, sprite){
  return (Math.abs(this.position.x - sprite.position.x) < px);
}

// Returns true if the enemy is within a certain
// number of pixels of the provided sprite on the y-axis
Enemy.prototype.isWithinYOf = function(px, sprite){
  return (Math.abs(this.position.y - sprite.position.y) < px);
}

// Returns true if the enemy is atleast a certain
// number of pixels from the provided sprite on the x-axis
Enemy.prototype.isAtleastXFrom = function(px, sprite){
  return (Math.abs(this.position.x - sprite.position.x) > px);
}

// Returns true if the enemy is atleast a certain
// number of pixels from the provided sprite on the y-axis
Enemy.prototype.isAtleastYFrom = function(px, sprite){
  return (Math.abs(this.position.y - sprite.position.x) > px);
}

// Can walk left without falling
// Returns true if the enemy can move left without falling
Enemy.prototype.canWalkLeftWithoutFalling = function(){
  if(this.body != null && this.platform != null)
    return (this.body.x > this.platform.body.x);
  else
    console.log('stuck');
  //return (this.floorCheckLeft.body.touching.down);
}

// Can walk right without falling
// Returns true if the enemy can walk right without falling
Enemy.prototype.canWalkRightWithoutFalling = function(){
  if(this.body != null && this.platform != null)
    return (this.body.right < this.platform.body.right);
  //return (this.floorCheckRight.body.touching.down);
}

// Walk
// Takes direction as -1 for left, 1 for right;
Enemy.prototype.walk = function(direction){
  this.animations.play('walk')
  this.scale.x = direction;
  this.body.velocity.x = (this.walkSpeed * direction);
  this.timers.walk = this.game.time.now + this.durations.walk;
}

// Jump
// Causes the enemy to jump up at a player
Enemy.prototype.jump = function(){
  //if(this.animations.currentAnim != "walk"){
    if(this.timers.jump < game.time.now){
      this.animations.play('jump');
      this.body.velocity.y = this.jumpSpeed;
      this.body.velocity.x = 75*this.body.facing;
    }
  //}
  this.timers.jump = this.game.time.now + this.durations.jump;
}

// Attack
// Causes the enemy to attack at a player
Enemy.prototype.attack = function(sprite){
  // TODO: Implement attack
}

// Idle
// Cause the enemy to stand and do nothing
Enemy.prototype.idle = function(){
  this.animations.play('idle');
  this.body.velocity.x = 0;
}

// Walk Towards
// Causes the enemy to walk towards the provided sprite
Enemy.prototype.walkTowards = function(sprite){
  console.log('walk towards player');
  console.log('left', this.canWalkLeftWithoutFalling());


}

Enemy.prototype.onFloor = function(){
    if(this.body.touching.down)
      return true;

    return false;
};
